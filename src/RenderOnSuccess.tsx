import React, { Fragment } from 'react'

interface Props {
   shouldRender: boolean
}

const RenderOnSuccess: React.FC<Props> = ({ children, shouldRender }) => {
   if (shouldRender) return <Fragment>{children}</Fragment>

   return null
}

export { RenderOnSuccess }
