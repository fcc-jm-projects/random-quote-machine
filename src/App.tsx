import 'modern-normalize/modern-normalize.css'
import './App.scss'

import React, { useEffect, useState } from 'react'
import SplitText from 'react-pose-text'

import { ReactComponent as Quote } from './quote-left-solid.svg'
import { RenderOnSuccess } from './RenderOnSuccess'
import { ReactComponent as Twitter } from './twitter-square-brands.svg'

const wordPoses = {
   exit: { opacity: 0 },
   enter: {
      opacity: 1,
      delay: ({ wordIndex }: { wordIndex: number }) => wordIndex * 40
   }
}

const App: React.FC = () => {
   const [quote, setQuote] = useState({ content: '', author: '' })
   const [colorIdx, setColorIdx] = useState(0)

   const fetchQuote = async () => {
      try {
         const response = await fetch('https://api.quotable.io/random')
         const quote = await response.json()

         setQuote(quote)
         // % 12 since we're using 13 statically declared colors
         setColorIdx(Math.floor(Math.random() * (12 - 1) + 1))
      } catch {
         console.error('Could not find quote!!')
      }
   }

   useEffect(() => {
      fetchQuote()
      // eslint-disable-next-line react-hooks/exhaustive-deps
   }, [])

   return (
      <div className={`base-background-color-${colorIdx} App--background`}>
         <RenderOnSuccess shouldRender={!!quote.content}>
            <section className={`base-color-${colorIdx} card`} id="quote-box">
               <blockquote className="quote">
                  <p className="quote__title" id="text">
                     <Quote className="quote__icon" />
                     <SplitText initialPose="exit" pose="enter" wordPoses={wordPoses}>
                        {quote.content}
                     </SplitText>
                  </p>
                  <footer className="quote__author" id="author">
                     <SplitText initialPose="exit" pose="enter" wordPoses={wordPoses}>
                        {`-- ${quote.author}`}
                     </SplitText>
                  </footer>
               </blockquote>
               <div className="buttons">
                  <a
                     href={`https://twitter.com/intent/tweet?text=${quote.content}  --${quote.author}`}
                     target="_blank"
                     rel="noopener noreferrer"
                     className={`base-color-${colorIdx}`}
                     id="tweet-quote"
                  >
                     <Twitter className={`buttons__twitter`} />
                  </a>
                  <button
                     className={`base-background-color-${colorIdx}  buttons__quote`}
                     id="new-quote"
                     type="button"
                     onClick={fetchQuote}
                  >
                     New Quote
                  </button>
               </div>
            </section>
            <section className="github">
               <a
                  href="https://gitlab.com/fcc-jm-projects/random-quote-machine"
                  target="_blank"
                  rel="noopener noreferrer"
                  className="github-link"
               >
                  Source Files
               </a>
            </section>
         </RenderOnSuccess>
      </div>
   )
}

export default App
