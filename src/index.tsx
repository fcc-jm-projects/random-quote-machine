import './index.scss'

import React, { Fragment } from 'react'
import ReactDOM from 'react-dom'
import { Helmet } from 'react-helmet'
import webFont from 'webfontloader'

import App from './App'
import * as serviceWorker from './serviceWorker'

webFont.load({
   google: {
      families: ['Oxygen']
   }
})

const WithHelmet = () => {
   return (
      <Fragment>
         <Helmet>
            <title>Random Quote Generator</title>
            <script src="https://cdn.freecodecamp.org/testable-projects-fcc/v1/bundle.js"></script>
         </Helmet>
         <App />
      </Fragment>
   )
}

ReactDOM.render(<WithHelmet />, document.getElementById('root'))

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister()
